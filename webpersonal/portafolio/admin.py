from django.contrib import admin
from .models import project

# Register your models here.

class projectAdmin(admin.ModelAdmin):
    readony_fields = ('created', 'updated') 
    
admin.site.register(project, projectAdmin)